#!/usr/bin/env python
import rospy
from finch_bot.srv import *
from blockext import *

class snap_server:

    @command("drive %m.direction", is_blocking=True, defaults=["forward"])
    def drive(self, direction):
        rospy.wait_for_service("finch_api")

        if (direction == "forward"):
            print "Commanding DRIVE_FWD"
            drive_cmd = FinchAPIRequest.DRIVE_FWD
        elif (direction == "left"):
            print "Commanding DRIVE_LEFT"
            drive_cmd = FinchAPIRequest.DRIVE_LEFT
        elif (direction == "right"):
            print "Commanding DRIVE_RIGHT"
            drive_cmd = FinchAPIRequest.DRIVE_RIGHT
        elif (direction == "stop"):
            print "Commanding DRIVE_STOP"
            drive_cmd = FinchAPIRequest.DRIVE_STOP
        else:
            print "Invalid direction: commanding DRIVE_STOP"
            drive_cmd = FinchAPIRequest.DRIVE_STOP

        try:
            finch_api = rospy.ServiceProxy("finch_api", FinchAPI)
            res = finch_api(drive_cmd)
            self.traffic_light_state = res.traffic_light_state
        except rospy.ServiceException, e:
            print "Service call failed: %s" % e


    # TODO: handle (better) the case that traffic_light is called before drive
    @reporter("traffic light")
    def traffic_light(self):
        if (not hasattr(self, 'traffic_light_state')):
            print "No traffic_light_state: returning NO_LIGHT"
            return "none"

        if (self.traffic_light_state == FinchAPIResponse.NO_LIGHT):
            print "Returning traffic_light_state: NO_LIGHT"
            return "none"
        elif (self.traffic_light_state == FinchAPIResponse.RED_LIGHT):
            print "Returning traffic_light_state: RED_LIGHT"
            return "red"
        elif (self.traffic_light_state == FinchAPIResponse.GREEN_LIGHT):
            print "Returning traffic_light_state: GREEN_LIGHT"
            return "green"
        else:
            print "Invalid traffic_light_state: returning error"
            return "error"

descriptor = Descriptor(
    name = "Finch Bot",
    port = 1234,
    blocks = get_decorated_blocks_from_class(snap_server),
    menus = dict(
        direction = ["forward", "stop", "left", "right"],
    )
)

extension = Extension(snap_server, descriptor)

if __name__ == "__main__":
    extension.run_forever(debug=True)
