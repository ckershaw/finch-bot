#!/usr/bin/env python
import rospy
import time
from finch_bot.srv import *

class finch_api(object):

    def api_cb(self, req):
        # process request
        if (req.drive_cmd == req.DRIVE_STOP):
            print "DRIVE_STOP"
        elif (req.drive_cmd == req.DRIVE_FWD):
            print "DRIVE_FWD"
        elif (req.drive_cmd == req.DRIVE_LEFT):
            print "DRIVE_LEFT"
        elif (req.drive_cmd == req.DRIVE_RIGHT):
            print "DRIVE_RIGHT"
        else:
            print "Unrecognized."

        # TODO: actually send drive command

        # generate response
        res = FinchAPIResponse()
        # TODO receive traffic light
        res.traffic_light_state = res.NO_LIGHT
        return res

    def __init__(self, *args, **kwds):

        rospy.init_node('finch_api', anonymous=True)
        rospy.Service('finch_api', FinchAPI, self.api_cb)
        #self.pub = rospy.Publisher('finch_follow', finch_follow_cmd, queue_size=10)

        rospy.spin()

    def __del__(self):
        pass

if __name__ == '__main__':
    fa = finch_api();
