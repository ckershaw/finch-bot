#!/usr/bin/env python
import rospy
import time
from sensor_msgs.msg import Joy
from finch_bot.msg import finch_mfrc522
from finch_bot.msg import finch_follow_cmd

# tune these
SPEED = 0.60;
TIME = 0.7;

#These are constants
MODE_STOP = finch_follow_cmd.MODE_STOP;
MODE_LINE = finch_follow_cmd.MODE_LINE;
MODE_DEAD = finch_follow_cmd.MODE_DEAD;

RED   = 0;
GREEN = 1;
BLUE  = 2;
CLEAR  = 3;

BTN_RED   = 1;
BTN_GREEN = 0;
BTN_BLUE  = 2;
BTN_CLEAR  = 3;

LEFT   = 0;
STRT = 1;
RIGHT  = 2;

COLOR_NAMES = ["RED", "GREEN", "BLUE"];

class finch_behavior_demo(object):


    def publish(self):
        fc = finch_follow_cmd();
        fc.mode = self.mode;
        fc.color_mode = self.color_mode;
        if(self.mode == MODE_DEAD):
            self.mode = MODE_LINE;
            fc.time = TIME;
            fc.left_mtr = SPEED;
            fc.right_mtr = SPEED;
            if(self.turn_mode == LEFT):
                fc.left_mtr = 0;
            if(self.turn_mode == RIGHT):
                fc.right_mtr = 0;

        self.pub.publish(fc);

    def mfrc_cb(self, msg):
        if(self.rfid != msg.uid):
            self.mode = MODE_STOP;

        self.rfid = msg.uid;

    def joy_cb(self, msg):

        if(msg.buttons[5]):
            self.mode = MODE_STOP;
        elif(msg.buttons[BTN_RED]):
            self.mode = MODE_LINE;
            self.color_mode = RED;
        elif(msg.buttons[BTN_GREEN]):
            self.mode = MODE_LINE;
            self.color_mode = GREEN;
        elif(msg.buttons[BTN_BLUE]):
            self.mode = MODE_LINE;
            self.color_mode = BLUE;
        elif(msg.axes[6] > 0.5):
            self.mode = MODE_DEAD;
            self.turn_mode = RIGHT;
        elif(msg.axes[6] < -0.5):
            self.mode = MODE_DEAD;
            self.turn_mode = LEFT;
        elif(msg.axes[7] > 0.5):
            self.mode = MODE_DEAD;
            self.turn_mode = STRT;
        else:
            return;


        self.publish();

    def __init__(self, *args, **kwds):

        self.rfid = -1;

        self.mode = MODE_STOP;
        self.color_mode = RED;
        self.turn_mode = LEFT;

        rospy.init_node('finch_behavior_demo', anonymous=True)

        self.pub = rospy.Publisher('finch_follow', finch_follow_cmd, queue_size=10)
        rospy.Subscriber('finch_mfrc', finch_mfrc522, self.mfrc_cb)
        rospy.Subscriber('joy', Joy, self.joy_cb)

        rospy.spin()

    def __del__(self):
        pass

if __name__ == '__main__':
    fd = finch_behavior_demo();
