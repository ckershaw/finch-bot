#!/usr/bin/env python
import rospy
import sys
import math
import yaml
from time import time
from finch_bot.msg import traffic_light_cmd
from visualization_msgs.msg import MarkerArray
from visualization_msgs.msg import Marker

all_pairs = {}
flashing_toggle_time = 0.7
normal_toggle_time = 5*1
traffic_light_pairs = rospy.get_param('traffic_light_pairs')

def set_pair_defaults(pair):
    pair['timestamp'] = time()
    if pair['status'] == 'flashing':
        pair['light1']['color'] = 'yellow'
        pair['light2']['color'] = 'red'
    if pair['status'] == 'normal':
        pair['light1']['color'] = 'green'
        pair['light2']['color'] = 'red'

def read_traffic_lights():
    global all_pairs
    global traffic_light_pairs
    all_pairs = {}
    for t in traffic_light_pairs:
        print t
        pair_dict = {}
        pair_dict['light1'] = t['light1'][0]
        pair_dict['light2'] = t['light2'][0]
        pair_dict['status'] = t['status']
        pair_dict['id'] = t['id']
        all_pairs[t['id']] = pair_dict
        set_pair_defaults(pair_dict)
    #print all_pairs
    return all_pairs

def toggle_flashing(pair):
    pair['timestamp'] = time()
    if pair['light1']['color'] == 'yellow':
        pair['light1']['color'] = 'yellow_black'
        pair['light2']['color'] = 'red_black'
    elif pair['light1']['color'] == 'yellow_black':
        pair['light1']['color'] = 'yellow'
        pair['light2']['color'] = 'red'
    elif pair['light1']['color'] == 'red':
        pair['light1']['color'] = 'red_black'
        pair['light2']['color'] = 'yellow_black'
    else:
        pair['light1']['color'] == 'red'
        pair['light2']['color'] = 'yellow'


def toggle_normal(pair):
    pair['timestamp'] = time()
    if pair['light1']['color'] == 'green':
        pair['light1']['color'] = 'red'
        pair['light2']['color'] = 'green'
    else:
        pair['light1']['color'] = 'green'
        pair['light2']['color'] = 'red'

# Checks if a pair needs toggling
def check_and_update_pair(pair):
    if pair['status'] == 'flashing':
        if time() - pair['timestamp'] > flashing_toggle_time:
            #print "Toggling"+str(pair['id'])
            toggle_flashing(pair)
    if pair['status'] == 'normal':
        if time() - pair['timestamp'] > normal_toggle_time:
            #print "Toggling"+str(pair['id'])
            toggle_normal(pair)

def create_traffic_light_marker(light):
    marker = Marker()
    marker.header.frame_id = "/world"
    marker.type = marker.SPHERE
    marker.action = marker.ADD
    marker.scale.x = 2
    marker.scale.y = 2
    marker.scale.z = 1
    marker.color.a = 1.0
    if light['color'] == 'yellow':
        marker.color.r = 1.0
        marker.color.g = 1.0
        marker.color.b = 0.0
    elif light['color'] == 'red_black':
        marker.color.r = 0.0
        marker.color.g = 0.0
        marker.color.b = 0.0
    elif light['color'] == 'yellow_black':
        marker.color.r = 0.0
        marker.color.g = 0.0
        marker.color.b = 0.0
    elif light['color'] == 'red':
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0
    elif light['color'] == 'green':
        marker.color.r = 0.0
        marker.color.g = 1.0
        marker.color.b = 0.0
    else:
        marker.color.r = 1.0
        marker.color.g = 1.0
        marker.color.b = 1.0

    marker.pose.position.x = light['x']
    marker.pose.position.y = light['y']
    marker.id = light['id']
    return marker

def main_loop():
    global all_pairs
    #publish rviz markers
    topic = 'traffic_light_markers'
    publisher = rospy.Publisher(topic, MarkerArray,queue_size=10)
    rospy.init_node('register')
    lightArray = MarkerArray()

    while not rospy.is_shutdown():
        for k in traffic_light_pairs:
            check_and_update_pair(all_pairs[k['id']])
            #update parameters
            #print "light1 color:" + all_pairs[k['id']]['light1']['color']
            #print "light2 color:" + all_pairs[k['id']]['light2']['color']

            k["light1"][0]['color'] = all_pairs[k['id']]['light1']['color']
            k["light2"][0]['color'] = all_pairs[k['id']]['light2']['color']

            #print "para light1 color:" + k["light1"][0]['color']
            #print "para light2 color:" + k["light2"][0]['color']

            #update markers
            m1 = create_traffic_light_marker(all_pairs[k['id']]['light1'])
            m2 = create_traffic_light_marker(all_pairs[k['id']]['light2'])
            lightArray.markers.append(m1)
            lightArray.markers.append(m2)

#        print "------------------"
        # Publish the MarkerArray
        publisher.publish(lightArray)
        rospy.sleep(0.1)

if __name__ == '__main__':
    try:
        read_traffic_lights()
        main_loop()
    except rospy.ROSInterruptException:
        pass
