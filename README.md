# README #

Project repo for DADARA/Ford/MEZ Autonomous Vehicle and Connected Infrastructure Workshop.

To get started, read the [Wiki](https://bitbucket.org/svozar/finch-bot/wiki/)

## INSTALLATION
You'll need ros already installed

Create a catkin ws:

$ mkdir catkin_foo

$ cd catkin_foo

$ mkdir src/

$ catkin_init_workspace src/

$ cd src/

$ git clone git@bitbucket.org:svozar/finch-bot.git

$ cd ../

$ catkin_make

## Messages
Put messages in the msg folder and be sure to add them
to CMakeLists.txt to get them to be updated

## BASH environments
be sure to source:

$ source catkin_foo/devel/setup.sh

this _could_ go in your .bashrc

